package com.ggs.cweta.testirovanie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.ggs.cweta.R;

public class Quiz extends AppCompatActivity {
    private final String STATE_PLAYER_NAME = "PLAYER_NAME";
    private final String STATE_SCORE = "SCORE";
    private final String STATE_Q1 = "STATE_Q1";
    private final String STATE_Q2 = "STATE_Q2";
    private final String STATE_Q4 = "STATE_Q4";
    private final String STATE_Q5 = "STATE_Q5";
    private final String STATE_Q8 = "STATE_Q8";
    private final String STATE_Q9 = "STATE_Q9";
    private final String STATE_Q10 = "STATE_Q10";
    private RadioGroup q1, q2, q3, q4, q8, q9, q5, q10;

    private RadioButton q1_1, q1_2,q1_3,q1_4,   q2_1, q2_2, q2_3,q2_4, q3_1,q3_2, q4_3,q4_4,q4_1,q4_2,  q5_4,q8_1,q8_2,q8_3,q8_4,q9_1,q9_2,q9_3,q9_4,q5_1,q5_2,q5_3,q10_4;
    private Spinner spinner_q6;
    private String playerName;
    private String[] correctAnswers;
    private int totalCorrect;
    private int questionNumber;



    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //save checked radio buttons
        savedInstanceState.putString(STATE_PLAYER_NAME, playerName);
        savedInstanceState.putInt(STATE_Q1, q1.getCheckedRadioButtonId());
        savedInstanceState.putInt(STATE_Q2, q2.getCheckedRadioButtonId());
        savedInstanceState.putInt(STATE_Q4, q3.getCheckedRadioButtonId());
//        savedInstanceState.putInt(STATE_Q5, q5.getCheckedRadioButtonId());
//        savedInstanceState.putInt(STATE_Q8, q8.getCheckedRadioButtonId());

        savedInstanceState.putInt(STATE_Q10, q10.getCheckedRadioButtonId());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        playerName = savedInstanceState.getString(STATE_PLAYER_NAME);
        //restore checked radio buttons
        try {
            q1.check(savedInstanceState.getInt(STATE_Q1));
            q2.check(savedInstanceState.getInt(STATE_Q2));
            q3.check(savedInstanceState.getInt(STATE_Q4));
            q4.check(savedInstanceState.getInt(STATE_Q5));
            q8.check(savedInstanceState.getInt(STATE_Q8));

            q10.check(savedInstanceState.getInt(STATE_Q10));
        } catch (Exception e) {
            Log.v(getString(R.string.quiz_activity_name), getString(R.string.LOG_ONRESTORE_ERROR));
        }

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        int layoutId = R.layout.activity_quiz;
        setContentView(layoutId);
        initialStates(getIntent());
    }
    private void initialStates(Intent intent) {
        playerName = intent.getStringExtra(STATE_PLAYER_NAME);
        totalCorrect = 0;
        questionNumber = 0;
        //RadioButton
        q1 = findViewById(R.id.q1);
        q2 = findViewById(R.id.q2);
        q3 = findViewById(R.id.q4);
        q4 = findViewById(R.id.q5);


        q10 = findViewById(R.id.q10);



        q1_2 = (RadioButton)findViewById(R.id.q1_2);
        q1_1 =(RadioButton) findViewById(R.id.q1_1);
                q1_3 =(RadioButton) findViewById(R.id.q1_3);
        q1_4 = (RadioButton)findViewById(R.id.q1_4);



                q2_1 = (RadioButton)findViewById(R.id.q2_1);
        q2_2 =(RadioButton) findViewById(R.id.q2_2);
                q2_3 = (RadioButton)findViewById(R.id.q2_3);
        q2_4 = (RadioButton)findViewById(R.id.q2_4);



                q3_1 = (RadioButton)findViewById(R.id.q4_1);
        q3_2 = (RadioButton)findViewById(R.id.q4_2);



                q4_1 = (RadioButton)findViewById(R.id.q5_1);
        q4_2 =(RadioButton) findViewById(R.id.q5_2);
                q4_3 = (RadioButton)findViewById(R.id.q5_3);
        q4_4 = (RadioButton)findViewById(R.id.q5_4);



                q5_1 = (RadioButton)findViewById(R.id.q10_1);
        q5_2 =(RadioButton) findViewById(R.id.q10_2);
                q5_3 = (RadioButton)findViewById(R.id.q10_3);
        q5_4 =(RadioButton) findViewById(R.id.q10_4);


    }



    private void initSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.q6_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_q6.setAdapter(adapter);
    }

    public void submit(View view) {
         totalCorrect = getTotalCorrectAnswers();

        Intent intent = new Intent(this, Result.class);
        intent.putExtra(STATE_PLAYER_NAME, playerName);
        intent.putExtra(STATE_SCORE, totalCorrect);
        startActivity(intent);
    }

    private int getTotalCorrectAnswers() {
        questionNumber = 0;

        try {


//            Весна
//            Женщина-весна обладательница прозрачной персиковой кожи. На щеках часто присутствует румянец.
//                    Она легко загорает, тело меняет оттенок от бледного на более темный.
//                    Цвет светлых глаз варьирует от василькового до чайного оттенка. Пышные волосы колора карамели или пшеницы подчеркивают нежность образа.
//
//                    Радужка глаз и кожа у мужчины-весны такие же, как у девушек. Но волосяной покров несколько темнее.
//                    Мужчина-весна, благодаря природным цветам внешности, выглядит моложе своих лет.

                        if (q2_2.isChecked()){
                totalCorrect=2;
            }  else if (q2_3.isChecked()&& q4_3.isChecked()&& q3_2.isChecked()){
                            totalCorrect=2;
                        }

//1
//                    Лето
//            Летние женщины неяркие, интеллигентные и утонченные. Среди русских летних девушек больше всего.
//            Тонкие, густые волосы светлого колора пепла обрамляют лицо с тонкой белой кожей.
//            Тело девушки-лета имеет легкий голубой подтон. Реже встречается оливковая кожа и только такая женщина-лето не боится загорать.
//                    Глаза серые, голубые, зеленые, но всегда среди других оттенков в радужке присутствует серый тон.


                    
            else if (q2_3.isChecked()){
                totalCorrect=1;
            }
                        else if (q2_2.isChecked() && q5_3.isChecked()){
                            totalCorrect=1;
                        }

//                    Осень
//            Яркие женщины, они всегда выделяются из толпы. Рыжие волосы часто бывают вьющимися.
//                    Светлая кожа, с просвечивающимся румянцем, иногда с веснушками. Но есть тип женщины-осень с телом оливкового цвета, в отличие от светлой кожи, она не боится загорать.
//                    Осеннему типу идут все цвета золотого времени года.
//                    Красный кирпичного оттенка, оливковый, томатный, охра – все оттенки желтеющей листвы гармоничны для девушки-осени.

  else if (q2_4.isChecked()){
                totalCorrect=3;
            }

            else{
                totalCorrect=4;
            }






//            if (q1_1.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//                totalCorrect=4;
//            } else if (q1_2.isChecked() && q2_2.isChecked()&& q3_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//                totalCorrect=2;
//            }else if (q1_3.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//                totalCorrect=1;
//            } else if (q1_4.isChecked() && q2_4.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//                totalCorrect=4; totalCorrect=3;
//            } else  if (q1_1.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_2.isChecked()&& q3_2.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_4.isChecked() && q2_4.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_1.isChecked() && q2_2.isChecked()&& q3_2.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_1.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_3.isChecked() && q2_4.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_2.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_1.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
//
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_2.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
//
//
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_2.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_2.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 4;}
//
//
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_2.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_2.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//
//
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//
//
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_2.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_2.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_2.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_2.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_1.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_1.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_1.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//
//            else  if (q1_1.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_1.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//
//
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_3.isChecked() && q2_2.isChecked() && q3_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_3.isChecked() && q2_2.isChecked() && q3_2.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 2;}
//
//            else  if (q1_3.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_3.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 2;}
//
//
//
//            else  if (q1_3.isChecked() && q2_2.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_3.isChecked() && q2_2.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 2;}
//
//            else  if (q1_3.isChecked() && q2_2.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_2.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
//            else  if (q1_3.isChecked() && q2_2.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_2.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 2;}
//
//            else  if (q1_3.isChecked() && q2_2.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_3.isChecked() && q2_2.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 2;}
//
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 1;}
//
////--------------------------- 86
//
//
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_2.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_2.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//
//
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_4.isChecked() && q2_3.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_4.isChecked() && q2_3.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 3;}
//
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//
//
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_2.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_2.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 3;}
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_2.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_2.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 3;}
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_2.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_2.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//
//
//
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 3;}
//
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//
//
//
//
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 3;}
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_4.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_4.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_4.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_4.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 3;}
//
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_1.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_4.isChecked() && q3_2.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//
//
//
//
//            if (q1_1.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;
//            } else if (q1_2.isChecked() && q2_2.isChecked()&& q3_2.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;
//            }else if (q1_3.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;
//            } else if (q1_4.isChecked() && q2_4.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;
//
//
//
//            } else  if (q1_1.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_2.isChecked()&& q3_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_4.isChecked() && q2_4.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_1.isChecked() && q2_2.isChecked()&& q3_1.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_1.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_3.isChecked() && q2_4.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_4.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_2.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_1.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_3.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_1.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 1;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//            else  if (q1_3.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
//
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_2.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//            else  if (q1_2.isChecked() && q2_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
////----------------161
//
//
//
//
//
//
//            else  if (q1_4.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_4.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 3;}
//
//
//
//
//            else  if (q1_3.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_3.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_3.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_3.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//
//            else  if (q1_4.isChecked() && q2_2.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
//            else  if (q1_4.isChecked() && q2_2.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
//
//
//            else  if (q1_4.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_4.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 3;}
//
//
//
//
//            else  if (q1_1.isChecked() && q2_4.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_4.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//
//
//
////----171
//
//
//
//
//
//            else  if (q1_2.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_2.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_3.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
//
//
//
//            else  if (q1_2.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_2.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//
//            else  if (q1_2.isChecked() && q2_4.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//
//            else  if (q1_2.isChecked() && q2_4.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 3;}
//
//
//
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_2.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//
//
//            else  if (q1_1.isChecked() && q2_1.isChecked()&& q3_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
//            else  if (q1_1.isChecked() && q2_1.isChecked()&& q3_2.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 2;}
//
//            else  if (q1_1.isChecked() && q2_2.isChecked()&& q3_1.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_1.isChecked() && q2_2.isChecked()&& q3_2.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//
//            else  if (q1_1.isChecked() && q2_2.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_2.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 4;}
//
//
//            else  if (q1_1.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//            else  if (q1_1.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_1.isChecked() && q5_1.isChecked()){
//
//                totalCorrect = 4;}
//
//
//
//            else  if (q1_1.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_1.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_2.isChecked() && q5_2.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_1.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_1.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_3.isChecked() && q5_3.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_1.isChecked() && q2_3.isChecked()&& q3_1.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 1;}
//
//            else  if (q1_1.isChecked() && q2_3.isChecked()&& q3_2.isChecked() && q4_4.isChecked() && q5_4.isChecked()){
//
//                totalCorrect = 1;}



//            else {
//                totalCorrect=4;
//            }








            return totalCorrect;

        } catch (Exception e) {
            Toast.makeText(this, R.string.validation_error, Toast.LENGTH_SHORT).show();
            return -1;

    }}

//    private void checkRadioButtonAnswer(RadioGroup rg) {
//        String answer = getCheckedRadioButtonId(rg).getText().toString();
//        if (answer.equals(correctAnswers[questionNumber]))
//            totalCorrect++;
//        questionNumber++;
//    }

    private RadioButton getCheckedRadioButtonId(RadioGroup rg) {
        return (RadioButton) findViewById(rg.getCheckedRadioButtonId());
    }

}
