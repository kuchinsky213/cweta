package com.ggs.cweta.act;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.ggs.cweta.MainActivity;
import com.ggs.cweta.R;

public class Vajnost extends AppCompatActivity {
Button vernutsya;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_vajnost);

        vernutsya = findViewById(R.id.back2);
        vernutsya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back = new Intent(Vajnost.this, MainActivity.class);
                startActivity(back);
                finish();
            }
        });

    }
}
